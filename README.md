# Kvittokvarnen

## Run locally

```bash
sudo apt install php7.4-cli php-sqlite3
php -S localhost:8000 -t src
```

## Create database

The first time you run, you have to create the database and an admin user:

```bash
php -d extension=sqlite3 -r "require 'src/controllers/db.php';create_database();add_user('admin', 'admin@foo.bar', 'somepassword', 1);"
```

