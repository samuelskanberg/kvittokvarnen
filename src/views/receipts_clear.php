<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
    if (!is_admin()) {
        header("Location: /");
        exit();
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        clear_receipts();
        $info = array(
            "message" => "Kvitton clearade",
            "type" => "info"
        );
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Cleara kvitton</h1>

    <?php if (isset($info)): ?>
        <?php
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        ?>
    <?php else: ?>
        Är du säker?
        <form method="post">
            <button type="submit" class="btn btn-primary mt-4">Cleara kvitton</button>
        </form>
    <?php endif; ?>
</div>

<?php include './components/page_end.php'; ?>
