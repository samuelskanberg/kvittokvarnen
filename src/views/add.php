<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
    $uid = session_get_uid();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sum = $_POST['sum'] ?? null;
        if ($sum != null && is_numeric($sum)) {
            $result = add_receipt($uid, $sum);
            if ($result) {
                $info = array(
                    "message" => "Kvitto tillagt: " . $sum . " kr",
                    "type" => "info"
                );
            } else {
                $info = array(
                    "message" => "Något fel uppstod",
                    "type" => "error"
                );
            }

        }
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Lägg till kvitto för <?php echo session_get_name(); ?></h1>
    <?php
        if (isset($info)) {
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        }
    ?>
    <form method="post">
        <div class="form-group">
            <label for="sum">Summa</label>
            <input type="number" class="form-control" name="sum">
        </div>
        <button type="submit" class="btn btn-primary mt-4">Lägg till</button>
    </form>

    <h2>Senast inlagda kvitton</h2>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Summa</th>
                <th scope="col">Datum</th>
                <th scope="col">Ta bort</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $receipts = list_receipts_for_user($uid);
                foreach($receipts as $receipt) {
                    echo "<tr>";
                    echo "<td>".$receipt['sum']."</td>";
                    echo "<td>".$receipt['date_added']."</td>";
                    echo '<td><a href="/delete/'.$receipt['id'].'">Ta bort '.$receipt['sum']."</td>";
                    echo "</tr>";
                }
            ?>
            <tr>
                <td><b><?php echo get_sum_receipts_for_user($uid); ?></b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<?php include './components/page_end.php'; ?>
