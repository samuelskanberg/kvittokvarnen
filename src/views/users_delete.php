<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
    if (!is_admin()) {
        header("Location: /");
        exit();
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $id = $_POST['user_delete_id'] ?? null;
        delete_user($id);
        $info = array(
            "message" => "Användaren borttagen",
            "type" => "info"
        );
    } else {
        $user = get_user($user_delete_id);
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Ta bort användare</h1>
    <?php if (isset($info)): ?>
        <?php
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        ?>
    <?php else: ?>
        <?php if ($user['id'] == null): ?>
            Den här användaren finns inte
        <?php else: ?>
            <table class="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Namn</th>
                    <th scope="col">Email</th>
                    <th scope="col">Admin</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $user['id']; ?></td>
                    <td><?php echo $user['name']; ?></td>
                    <td><?php echo $user['email']; ?></td>
                    <td><?php echo $user['admin']; ?></td>
                </tr>
            </tbody>
        </table>
            <form method="post">
                <input type="hidden" class="form-control" name="user_delete_id" value="<?php echo $user_delete_id; ?>">
                <button type="submit" class="btn btn-primary mt-4">Ta bort</button>
            </form>
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php include './components/page_end.php'; ?>