<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
    if (!is_admin()) {
        header("Location: /");
        exit();
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $uid = $_POST['user_edit_id'] ?? null;
        $name = $_POST['name'] ?? null;
        $email = $_POST['email'] ?? null;
        $is_admin = strcmp($_POST['admin'] ?? "", "") != 0;

        update_user($uid, $name, $email, $is_admin);

        $infos = array();
        $infos[] = array(
            "message" => "Användaren uppdaterad",
            "type" => "info"
        );

        $password1 = $_POST['password1'] ?? "";
        $password2 = $_POST['password2'] ?? "";
        if (strlen($password1) > 0) {
            if (strcmp($password1, $password2) != 0) {
                $infos[] = array(
                    "message" => "Lösenorden stämmer inte överens",
                    "type" => "error"
                );
            } else {
                update_password($uid, $password1);
                $infos[] = array(
                    "message" => "Lösenord uppdaterat",
                    "type" => "info"
                );          
            }
        }

    } 
    $user = get_user($user_edit_id);
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Ändra användare</h1>
    <?php if (isset($infos)): ?>
        <?php
            foreach ($infos as $info) {
                if ($info['type'] == "error") {
                    echo '<div class="alert alert-danger">';
                } else {
                    echo '<div class="alert alert-success">';
                }
                echo $info['message'];
                echo '</div>';
            }
        ?>
    <?php endif; ?>
    <?php if ($user['id'] == null): ?>
        Den här användaren finns inte
    <?php else: ?>
        <form method="post">
            <input type="hidden" class="form-control" name="user_edit_id" value="<?php echo $user_edit_id ?>">

            <div class="form-group">
                <label for="name">Namn</label>
                <input type="text" class="form-control" name="name" value="<?php echo $user['name']; ?>">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" value="<?php echo $user['email']; ?>">
            </div>
            <div class="form-group">
                <label for="admin">Admin</label>
                <input type="checkbox" class="form-check-input" name="admin" value="admin" <?php if ($user['admin']) echo "checked"; ?>>
            </div>
            <div class="form-group">
                <label for="password1">Lösenord</label>
                <input type="password" class="form-control" name="password1">
            </div>
            <div class="form-group">
                <label for="password2">Lösenord igen</label>
                <input type="password" class="form-control" name="password2">
            </div>
            <button type="submit" class="btn btn-primary mt-4">Uppdatera</button>
        </form>
    <?php endif; ?>
</div>

<?php include './components/page_end.php'; ?>