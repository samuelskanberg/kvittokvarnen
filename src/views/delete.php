<?php
    $uid = session_get_uid();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $id = $_POST['delete_id'] ?? null;
        $receipt = get_receipt($id, $uid);
        if ($receipt['id'] != null) {
            $res = delete_receipt($receipt['id'], $uid);
            $info = array(
                "message" => "Kvittot borttaget",
                "type" => "info"
            );
        }
    } else {
        $receipt = get_receipt($delete_id, $uid);
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Ta bort kvitto</h1>
    <?php if (isset($info)): ?>
        <?php
            if ($info['type'] == "error") {
                echo '<div class="alert alert-danger">';
            } else {
                echo '<div class="alert alert-success">';
            }
            echo $info['message'];
            echo '</div>';
        ?>
        <a href="/add">Lägg till fler kvitton</a>
    <?php else: ?>
        <?php if ($receipt['id'] == null): ?>
            Det här kvittot finns inte.
        <?php else: ?>
            <table class="table">
            <thead>
                <tr>
                    <th scope="col">Summa</th>
                    <th scope="col">Datum</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $receipt['sum']; ?> kr</td>
                    <td><?php echo $receipt['date_added']; ?></td>
                </tr>
            </tbody>
        </table>
            <form method="post">
                <input type="hidden" class="form-control" name="delete_id" value="<?php echo $delete_id; ?>">
                <button type="submit" class="btn btn-primary mt-4">Ta bort</button>
            </form>
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php include './components/page_end.php'; ?>