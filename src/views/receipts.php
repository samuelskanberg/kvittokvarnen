<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
?>

<?php include './components/page_start.php'; ?>
<?php include './components/header.php'; ?>
<div class="container">
    <h1>Allas kvitton</h1>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Namn</th>
                <th scope="col">Handlat för</th>
                <th scope="col">Antal kvitton</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $users = list_users();
                foreach($users as $user) {
                    echo "<tr>";
                    echo "<td>".$user['name']."</td>";
                    echo "<td>".get_sum_receipts_for_user($user['id'])."</td>";
                    echo "<td>".get_number_receipts_for_user($user['id'])."</td>";
                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>
    <?php if (is_admin()): ?>
        <a href="/receipts_csv" class="btn btn-primary mt-4">Exportera csv</a>
        <a href="/receipts_clear" class="btn btn-primary mt-4">Clear</a>
    <?php endif; ?>

</div>


<?php include './components/page_end.php'; ?>
