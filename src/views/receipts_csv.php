<?php
    if (!is_logged_in()) {
        header("Location: /login");
        exit();
    }
?>

<?php

/*Let's create a csv file in the following format:

Ellen;Samuel;Leo;Martin
300;200;200;300
400;300;100;300
400;300;;300
300;;;200
;;;100
;;;100

*/

$users = list_users();
$header = array();
$users_receipts = array();
foreach ($users as $user) {
    $header[] = $user['name'];
    $users_receipts[] = list_receipts_for_user($user['id']);
}

$i = 0;
$rows = array();
$rows[] = $header;
while (true) {
    $row = array();
    $no_more_receipts = true;
    for ($user_index = 0; $user_index < count($header); $user_index++) {
        if ($i < count($users_receipts[$user_index])) {
            $row[$user_index] = $users_receipts[$user_index][$i]['sum'];
            $no_more_receipts = false;
        } else {
            $row[$user_index] = '';
        }
    }
    if ($no_more_receipts) {
        break;
    } else {
        $rows[] = $row;
    }
    $i++;
}


header('Content-type: text/csv');
header('Content-Disposition: attachment; filename="filename.csv"');

$output = fopen('php://output', 'w');

foreach ($rows as $fields) {
    fputcsv($output, $fields);
}

?>