<?php

$db_path = __DIR__ . "/kvittokvarnen.db";

function create_database() {
    global $db_path;
    $db = new SQLite3($db_path);

    $db->exec("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, email NOT NULL UNIQUE, name TEXT, password TEXT, admin INTEGER)");
    $db->exec("CREATE TABLE IF NOT EXISTS receipts(id INTEGER PRIMARY KEY, uid INT, sum INT, date_added TEXT, date_cleared TEXT)");
    $db->close();
}

function list_users() {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, name, email, admin from users');
    $res = $stm->execute();
    $users = array();
    while ($row = $res->fetchArray()) {
        $users[] = array(
            "id" => $row['id'],
            "name" => $row['name'],
            "email" => $row['email'],
            "admin" => $row['admin']
        );
    }
    return $users;
}

function add_user($name, $email, $password, $is_admin) {
    global $db_path;
    $db = new SQLite3($db_path);

    $hashed_password = password_hash($password, PASSWORD_BCRYPT);

    if ($is_admin) {
        $admin = 1;
    } else {
        $admin = 0;
    }
    $stm = $db->prepare('INSERT INTO users(email, name, password, admin) VALUES (:email, :name, :password, :admin)');
    $stm->bindValue(':email', $email);
    $stm->bindValue(':name', $name);
    $stm->bindValue(':password', $hashed_password);
    $stm->bindValue(':admin', $admin);
    $res = $stm->execute();

    return boolval($res);
}

function update_user($uid, $name, $email, $is_admin) {
    global $db_path;
    $db = new SQLite3($db_path);

    if ($is_admin) {
        $admin = 1;
    } else {
        $admin = 0;
    }
    $stm = $db->prepare('UPDATE users set email = :email, name = :name, admin = :admin where id = :id');
    $stm->bindValue(':email', $email);
    $stm->bindValue(':name', $name);
    $stm->bindValue(':admin', $admin);
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();

    return boolval($res);
}

function update_password($uid, $password) {
    global $db_path;
    $db = new SQLite3($db_path);

    $hashed_password = password_hash($password, PASSWORD_BCRYPT);

    $stm = $db->prepare('UPDATE users set password = :password where id = :id');
    $stm->bindValue(':password', $hashed_password);
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();

    return boolval($res);
}

function delete_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('DELETE FROM users where id = :id');
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();

    return boolval($res);
}

function get_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, name, email, admin from users where id = :id');
    $stm->bindValue(':id', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $user = array(
        "id" => $row['id'] ?? null,
        "name" => $row['name'] ?? null,
        "email" => $row['email'] ?? null,
        "admin" => $row['admin'] ?? null
    );
    return $user;
}

function verify_password($email, $password) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, name, password, admin from users where email = :email');
    $stm->bindValue(':email', $email);
    $res = $stm->execute();
    $row = $res->fetchArray();

    if (!$row) {
        return false;
    }

    $password_hash = $row['password'] ?? null;
    $uid = $row['id'] ?? null;
    $name = $row['name'] ?? null;
    $is_admin = boolval(intval($row['admin'] ?? 0) == 1);
    $password_match = password_verify($password, $password_hash);

    return array(
        "password_match" => $password_match,
        "uid" => $uid,
        "name" => $name,
        "is_admin" => $is_admin
    );
}

function add_receipt($uid, $sum) {
    global $db_path;
    $db = new SQLite3($db_path);

    $date_added = date("Y-m-d");

    $stm = $db->prepare('INSERT INTO receipts(uid, sum, date_added) VALUES (:uid, :sum, :date_added)');
    $stm->bindValue(':uid', $uid);
    $stm->bindValue(':sum', $sum);
    $stm->bindValue(':date_added', $date_added);
    $res = $stm->execute();

    return boolval($res);
}

function get_receipt($id, $uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, uid, sum, date_added from receipts where id = :id and uid = :uid');
    $stm->bindValue(':id', $id);
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $receipt = array(
        "id" => $row['id'] ?? null,
        "sum" => $row['sum'] ?? null,
        "date_added" => $row['date_added'] ?? null
    );
    return $receipt;
}

function delete_receipt($id, $uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('DELETE from receipts where id = :id and uid = :uid');
    $stm->bindValue(':id', $id);
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    // TODO: Check if it worked
}

function list_receipts_for_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT id, sum, date_added from receipts where uid = :uid and date_cleared is NULL order by id desc');
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $receipts = array();
    while ($row = $res->fetchArray()) {
        $receipts[] = array(
            "id" => $row['id'],
            "sum" => $row['sum'],
            "date_added" => $row['date_added']
        );
    }
    return $receipts;
}

function clear_receipts() {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('UPDATE receipts set date_cleared = :date_cleared where date_cleared is null');
    $date_cleared = date('Y-m-d');
    $stm->bindValue(':date_cleared', $date_cleared);
    $res = $stm->execute();
}

function get_sum_receipts_for_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT sum(sum) as total_sum from receipts where uid = :uid and date_cleared is NULL');
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $sum = $row['total_sum'];
    return $sum;
}

function get_number_receipts_for_user($uid) {
    global $db_path;
    $db = new SQLite3($db_path);

    $stm = $db->prepare('SELECT count(sum) as total_count from receipts where uid = :uid and date_cleared is NULL');
    $stm->bindValue(':uid', $uid);
    $res = $stm->execute();
    $row = $res->fetchArray();
    $count = $row['total_count'];
    return $count;
}

?>