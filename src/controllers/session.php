<?php

function is_logged_in() {
    return isset($_SESSION['uid']);
}

function is_admin() {
    return $_SESSION['is_admin'] ?? false;
}

function session_login($name, $uid, $is_admin) {
    $_SESSION['uid'] = $uid;
    $_SESSION['name'] = $name;
    $_SESSION['is_admin'] = $is_admin;
}

function session_get_uid() {
    return $_SESSION['uid'];
}

function session_get_name() {
    return $_SESSION['name'];
}

function session_logout() {
    unset($_SESSION["name"]);
    unset($_SESSION["uid"]);
    unset($_SESSION["is_admin"]);
}

?>