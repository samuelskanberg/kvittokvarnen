<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Kvittokvarnen</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
            <?php if (is_logged_in()): ?>
              <li class="nav-item">
                <a class="nav-link" href="/add">Lägg till kvitto</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/receipts">Allas kvitton</a>
              </li>
              <?php if (is_admin()): ?>
                <li class="nav-item">
                  <a class="nav-link" href="/users">Användare</a>
                </li>
              <?php endif; ?>
              <li class="nav-item">
                <a class="nav-link" href="/logout">Logga ut</a>
              </li>
            <?php else: ?>
              <li class="nav-item">
                <a class="nav-link" href="/login">Logga in</a>
              </li>
            <?php endif; ?>
        </ul>
      </div>
  </div>
</nav>
